#include <fstream>
#include <iomanip>
#include <iostream>
#include <locale.h>
#include <string>

using namespace std;

#include "parser.h"

size_t count = 0;

vector<size_t> find_pattern(const string &text, const string &patt)
{
    size_t l = 0;
    vector<size_t> positions;

    do
    {
        l = text.find(patt,l);
        if (l != string::npos)
        {
            count++;
            positions.push_back(l);
            l = l + patt.size();
        }
    } while (l != string::npos);

    return positions;
}

string subs_patterns(const string &text, const string &patt, const string &corr_patt, const vector<size_t> patt_pos)
{
    char ch;
    int i = 0;
    size_t l = 0, ppos = 0;
    string corr_text = "", auxstr = "";
    stringstream iss(text.c_str());

    ppos = patt_pos[i];
    do 
    {
        if (l == ppos)
        {
            l += patt.size();
            i++;
            corr_text += corr_patt;
            for (int i = 0; i < patt.size(); i++) {iss.get(ch);}
        } else
        {
            iss.get(ch);
            auxstr = ch;
            corr_text += auxstr;
            l++;
        }
        if (i < patt_pos.size())
        {
            ppos = patt_pos[i];
        } else
        {
            ppos = text.size() + 1;
        }
    } while (l < text.size());

    return corr_text;
}

int main(int argc, char* argv[])
{
    // char variables
    char delim = '\\';

    // string class objects
    string text = "", corr_text = "", line = "", pline = "", p = "", cp = "";
    string infname = argv[1], outfname = argv[2];

    // ifstream class objects
    ifstream infile(infname.c_str()), inpfile("patterns.txt");

    // ofstream class objects
    ofstream outfile(outfname.c_str()), outpfile("patterns_new.txt");
    ofstream test_file("merda.txt");

    // vector class objects
    vector<string> patt, corr_patt;
    vector<size_t> pp;

    // parser class objects
    parser patterns;

    setlocale(LC_ALL,"");

    getline(inpfile,pline);
    patterns.re_init(pline);
    outpfile << "Patterns = ";
    outpfile << patterns.ntoken() << '\n';
    for (int i = 0; i < patterns.ntoken(); i++) {patt.push_back(patterns.select_token(i));}
    patt.push_back("\\c c");

    getline(inpfile,pline);
    patterns.re_init(pline);
    outpfile << "Corrected patterns = ";
    outpfile << patterns.ntoken() << '\n';
    for (int j = 0; j < patterns.ntoken(); j++) {corr_patt.push_back(patterns.select_token(j));}
    corr_patt.push_back("�");

    for (int k = 0; k < patterns.ntoken(); k++) {outpfile << patt[k] << " = " << corr_patt[k] << '\n';}

    inpfile.close();
    outpfile.close();

    while (getline(infile,line))
    {
        text = line;
        test_file << "========================================================================" << '\n';
        test_file << text << '\n';
        test_file << "========================================================================" << '\n';
        for (int ipatt = 0; ipatt < patt.size(); ipatt++)
        {
            if (ipatt == 2)
            {
                p = patt[ipatt]+" ";
            } else
            {
                p = patt[ipatt];
            }
            cp = corr_patt[ipatt];
            pp = find_pattern(text,p);
            test_file << count << "   " << p;
            for (int crap = 0; crap < pp.size(); crap++) {test_file << "   " << pp[crap];}
            test_file << '\n';
            if (count > 0)
            {
                corr_text = subs_patterns(text, p, cp, pp);
                text = corr_text;
            } else {corr_text = text;}
            test_file << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << '\n';
            count = 0;
            pp.clear();
        }
        test_file << "========================================================================" << '\n';
        outfile << corr_text << '\n';
    }

    infile.close();
    outfile.close();

    return 0;
}
